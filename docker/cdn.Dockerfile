FROM docker.io/library/httpd:latest 

RUN rm -r /usr/local/apache2/htdocs/
COPY ./static/ /usr/local/apache2/htdocs/
