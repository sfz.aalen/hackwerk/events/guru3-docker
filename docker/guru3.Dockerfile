FROM harbor.aalen.space/sfz.aalen/hackwerk/events/guru3-docker-guru-base:latest

CMD ["daphne", "--bind", "0.0.0.0", "--port", "8080", "--verbosity", "0", "guru3.asgi:application"]

# Image Stuff
EXPOSE 8080
VOLUME [ "/data" ]
