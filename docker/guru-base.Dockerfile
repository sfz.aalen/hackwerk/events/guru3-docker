FROM docker.io/library/python:3.10
# Install Requirements
COPY guru/requirements.txt .
RUN pip3 install -r requirements.txt

# Install WGSY
RUN apt-get update && apt-get install -y wkhtmltopdf

# Copy GURU3
COPY guru/ /app/
WORKDIR /app/
