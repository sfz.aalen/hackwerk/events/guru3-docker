FROM harbor.aalen.space/sfz.aalen/hackwerk/events/guru3-docker-guru-base:latest

CMD ["celery", "-A", "guru3", "worker", "--loglevel=INFO"]
